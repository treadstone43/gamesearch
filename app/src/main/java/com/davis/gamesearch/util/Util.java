package com.davis.gamesearch.util;

import java.util.ArrayList;

/**
 * A class to hold commonly used methods (i.e. Checking if a string is either null OR empty)
 */
public class Util {

    public static boolean isNullOrEmpty(final String s) {
        boolean output = false;

        if (s == null || s.trim().length() == 0) {
            output = true;
        }
        return output;
    }

    public static boolean isNotNullOrEmpty(final String s) {
        return !isNullOrEmpty(s);
    }

    public static boolean isNullOrEmpty(final ArrayList list) {
        return list == null || list.size() == 0;
    }

    public static boolean isNotNullOrEmpty(final ArrayList list) {
        return !isNullOrEmpty(list);
    }

}
