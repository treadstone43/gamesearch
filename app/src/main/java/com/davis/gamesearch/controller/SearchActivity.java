package com.davis.gamesearch.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.SearchView;

import com.davis.gamesearch.R;
import com.davis.gamesearch.giantbomb.GiantBombSearchEngine;
import com.davis.gamesearch.network.Result;
import com.davis.gamesearch.network.SearchEngine;
import com.davis.gamesearch.util.Util;

import java.util.ArrayList;


public class SearchActivity extends Activity implements SearchEngine.Listener, ResultsFragment.ResultsListener {

    public static final String RESULTS_KEY = "ResultsParcelableKey";
    private static final String TAG = SearchActivity.class.getSimpleName();
    private static final String RESULTS_FRAGMENT = "ResultsFragmentTag";
    private static final String LAST_QUERY = "LastQuery";
    private static final String RECEIVED_LAST_RESPONSE = "ReceivedLastSearchResponse";
    private ArrayList<Result> results;
    private ProgressDialog progressDialog = null;
    private SearchView searchView;
    private String query;
    private boolean receivedLastSearchResponse = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(R.id.container, new ResultsFragment(), RESULTS_FRAGMENT).commit();
        } else {
            query = savedInstanceState.getString(LAST_QUERY);
            results = savedInstanceState.getParcelableArrayList(RESULTS_KEY);
            receivedLastSearchResponse = savedInstanceState.getBoolean(RECEIVED_LAST_RESPONSE);
            if (!receivedLastSearchResponse) {
                //This is to prevent state loss when the screen changes orientation.
                Log.d(TAG, "Did not receive last search response. Search again.");
                performSearch(query);
            }
        }
        Log.d(TAG, "onCreateFinished");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        Log.d(TAG, "Query Entered");

        // Accept the query from the user and start the search.
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String newQuery = intent.getStringExtra(SearchManager.QUERY);
            performSearch(newQuery);
        }
    }

    private void performSearch(String newQuery) {
        Log.d(TAG, "Query: " + newQuery);
        receivedLastSearchResponse = false;
        this.query = newQuery;
        SearchEngine engine = new GiantBombSearchEngine(this);
        engine.fetchResults(newQuery);
        showLoadingSpinner();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        //Setup the Search Manager
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search_view).getActionView();
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
        searchView.setSearchableInfo(searchableInfo);

        return true;
    }

    /**
     * SearchEngine Listener Method. Receives the results
     * Since I am performing a fragment transaction inside a callback, I need to be careful
     * of state loss. todo: Avoid performing a fragment transaction inside a callback.
     * @param results
     */
    @Override
    public void searchComplete(ArrayList<Result> results) {
        this.results = results;
        receivedLastSearchResponse = true;
        ResultsFragment fragment = (ResultsFragment) getFragmentManager().findFragmentByTag(RESULTS_FRAGMENT);
        hideLoadingSpinner();
        if (fragment != null) {
            //update the fragment
            fragment.updateResultsList(results);
        } else {
            //create a new one
            // commitAllowingStateLoss is used due to a reported bug in the support package (http://stackoverflow.com/a/10261449/1424464)
            try {
                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(R.id.container, new ResultsFragment(), RESULTS_FRAGMENT);
                transaction.commit();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Search Engine Listener Method. Receives any errors.
     *
     * @param error
     */
    @Override public void searchError(String error) {
        hideLoadingSpinner();
        receivedLastSearchResponse = true;
        String message = (Util.isNullOrEmpty(error) ? "Failure in response." : error);
        //Show error message
        new AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage(message)
                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        hideLoadingSpinner();

        // Save the user's current game state
        savedInstanceState.putParcelableArrayList(RESULTS_KEY, results);
        savedInstanceState.putString(LAST_QUERY, query);
        savedInstanceState.putBoolean(RECEIVED_LAST_RESPONSE, receivedLastSearchResponse);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * ResultsFragment Listener Method. Receives the 'Result' selected from the list.
     * @param result
     */
    @Override
    public void resultSelected(Result result) {
        Intent intent = new Intent(this, GameDetailActivity.class);
        intent.putExtra(GameDetailActivity.RESULT_KEY, result);
        startActivity(intent);
    }

    /**
     * Spinner Method. Shows spinner while search is being performed.
     */
    private void showLoadingSpinner() {
        progressDialog = ProgressDialog.show(this, null, getString(R.string.progress_dialog));
        progressDialog.setCancelable(false);
    }

    /**
     * Spinner Method. Hides the spinner after results/error has been recieved.
     */
    private void hideLoadingSpinner() {

        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            e.printStackTrace();
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            this.progressDialog = null;
        }
    }

}
