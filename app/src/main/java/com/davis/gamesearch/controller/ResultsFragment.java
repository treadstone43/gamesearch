package com.davis.gamesearch.controller;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.davis.gamesearch.R;
import com.davis.gamesearch.network.Result;
import com.davis.gamesearch.network.imageloader.ImageDownloadTask;
import com.davis.gamesearch.network.imageloader.ImageHolder;
import com.davis.gamesearch.util.Util;

import java.util.ArrayList;

public class ResultsFragment extends Fragment implements ImageDownloadTask.ImageDownloadListener {

    private static final String TAG = ResultsFragment.class.getSimpleName();

    private ListView resultsListView;
    private ResultsListAdapter listAdapter;
    private ArrayList<Result> results;
    private ResultsListener resultListener;

    /**
     * Allows this fragment to communicate with its parent activity(Search Activity).
     */
    interface ResultsListener {
        public void resultSelected(Result result);
    }

    public ResultsListener getResultListener() {
        return resultListener;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            //force the parent activity to implement this interface
            resultListener = (ResultsListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ResultsListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        resultsListView = (ListView) rootView.findViewById(R.id.resultsList);
        resultsListView.setDividerHeight(0);
        listAdapter = new ResultsListAdapter(this);
        resultsListView.setAdapter(listAdapter);

        if (savedInstanceState != null) {
            results = savedInstanceState.getParcelableArrayList(SearchActivity.RESULTS_KEY);
        }

        if (Util.isNotNullOrEmpty(results)) {
            updateResultsList(results);
        }

        //Set background image for fragment based on orientation
        Resources res = getResources();
        Drawable portrait = res.getDrawable(R.drawable.giant_bomb);
        Drawable landscape = res.getDrawable(R.drawable.giant_bomb_landscape);

        WindowManager window = (WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE);
        Display display = window.getDefaultDisplay();
        int num = display.getRotation();
        if (num == 0) {
            rootView.setBackground(portrait);
        } else if (num == 1 || num == 3) {
            rootView.setBackground(landscape);
        } else {
            rootView.setBackground(portrait);
        }

        return rootView;
    }


    /**
     * Called by the parent activity there are new Results to display
     *
     * @param results
     */
    public void updateResultsList(ArrayList<Result> results) {
        Log.d(TAG, "finished search. update results list.");
        this.results = results;
        if (listAdapter != null) {
            listAdapter.setResults(results);
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putParcelableArrayList(SearchActivity.RESULTS_KEY, results);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * ResultsListAdapter
     * List adapter for all the search results
     */
    private class ResultsListAdapter extends BaseAdapter {

        private ArrayList<Result> results;
        private final ResultsFragment fragment;

        public ResultsListAdapter(ResultsFragment frag) {
            results = new ArrayList<Result>();
            this.fragment = frag;
        }


        @Override public int getCount() {
            return results.size();
        }

        @Override public Result getItem(int position) {
            return results.get(position);
        }

        @Override public long getItemId(int position) {
            return position;
        }

        @Override public View getView(int i, View view, ViewGroup viewGroup) {
            final Result result = getItem(i);
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_results, null);

            TextView nameTextView = (TextView) view.findViewById(R.id.name);
            nameTextView.setText(result.getName());

            TextView dateCreated = (TextView) view.findViewById(R.id.dateCreated);
            dateCreated.setText("Released: " + result.getReleaseDate());

            final ImageView imageView = (ImageView) view.findViewById(R.id.image);
            ImageHolder holder = new ImageHolder(result, imageView);
            loadBitmap(holder);

            view.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    fragment.getResultListener().resultSelected(result);
                }
            });

            return view;
        }

        public void setResults(ArrayList<Result> results) {
            this.results = results;
        }

    }

    /**
     * Attempts to retrieve the bitmap from the Cache. If it's not there then it downloads it.
     *
     * @param holder
     */
    public void loadBitmap(ImageHolder holder) {
        String key = holder.getResultKey();
        Bitmap bitmap = BitmapCache.getInstance().getBitmapFromMemCache(key);
        if (bitmap == null) {
            Log.d(TAG, "BITMAP: DOWNLOADING: " + key);
            ImageDownloadTask task = new ImageDownloadTask(this);
            task.execute(holder);
        } else {
            Log.d(TAG, "BITMAP: LOADING FROM CACHE: " + key);
            holder.getImageView().setImageBitmap(bitmap);
        }
    }

    /**
     * When ImageDownloadTask finishes downloading a bitmap, it returns it here.
     *
     * @param bitmap
     * @param holder
     */
    @Override
    public void downloadComplete(Bitmap bitmap, ImageHolder holder) {
        ImageView imageView = holder.getImageView();

        if (bitmap == null) {
            Log.e("ImageDownloadTask", "setting placeholder");
            imageView.setImageResource(R.drawable.placeholder);
        } else {
            Log.v("ImageDownloadTask", "setting bitmap");
            imageView.setImageBitmap(bitmap);
            //Save the bitmap to the cache
            BitmapCache.getInstance().addBitmapToMemoryCache(holder.getResultKey(), bitmap);
        }
    }


}