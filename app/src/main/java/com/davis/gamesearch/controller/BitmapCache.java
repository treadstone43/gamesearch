package com.davis.gamesearch.controller;

import android.graphics.Bitmap;
import android.util.LruCache;

public class BitmapCache {

    private static BitmapCache instance = null;

    private LruCache<String, Bitmap> bitmapCache;

    public static BitmapCache getInstance() {
        if (instance == null) {
            instance = new BitmapCache();
        }
        return instance;
    }

    private BitmapCache() {
        setupCache();
    }

    /**
     * //todo: Figure out best time to call this.
     */
    private void clearCache() {
        bitmapCache.evictAll();
    }

    private void setupCache() {
        //Create the bitmapCache and set its memory to 1/8 of the max memory available.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        bitmapCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    /**
     * Bitmap Caching
     * Used to store the bitmaps in the cache
     *
     * @param key
     * @param bitmap
     */
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (bitmapCache == null) {
            setupCache();
        }
        if (getBitmapFromMemCache(key) == null) {
            bitmapCache.put(key, bitmap);
        }
    }

    /**
     * Bitmap Caching
     * Used to retrieve the bitmaps in order to rebuild the UI.
     *
     * @param key
     * @return
     */
    public Bitmap getBitmapFromMemCache(String key) {
        if (bitmapCache == null) {
            setupCache();
        }
        return bitmapCache.get(key);
    }
}
