package com.davis.gamesearch.controller;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.davis.gamesearch.R;
import com.davis.gamesearch.network.Result;
import com.davis.gamesearch.network.imageloader.ImageDownloadTask;
import com.davis.gamesearch.network.imageloader.ImageHolder;
import com.davis.gamesearch.util.Util;

import java.util.ArrayList;

public class GameDetailActivity extends Activity implements ImageDownloadTask.ImageDownloadListener {

    public static final String RESULT_KEY = "ResultParcelableKey";
    private static final String TAG = GameDetailActivity.class.getSimpleName();
    private Result result;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if (getIntent().hasExtra(RESULT_KEY)) {
            result = getIntent().getExtras().getParcelable(RESULT_KEY);
            Log.d(TAG, "Received result: " + result.getName());
        }
        renderView();
    }

    private void renderView() {
        TextView nameView = (TextView) findViewById(R.id.gameName);
        TextView descriptionView = (TextView) findViewById(R.id.description);
        TextView releaseDate = (TextView) findViewById(R.id.gameRelease);
        TextView platformListingView = (TextView) findViewById(R.id.platformListing);
        final ImageView imageView = (ImageView) findViewById(R.id.gameImage);

        nameView.setText(result.getName());
        descriptionView.setText(result.getDescription());
        releaseDate.setText(Util.isNullOrEmpty(result.getReleaseDate()) ? "" : result.getReleaseDate());

        //Load Image
        ImageHolder holder = new ImageHolder(result, imageView);
        loadImage(holder);

        //Setup scrollview
        ArrayList<String> platforms = result.getPlatforms();
        String allplatforms = "";
        if (Util.isNotNullOrEmpty(platforms)) {
            for (String platform : platforms) {
                allplatforms += platform + ", ";
            }
        } else {
            allplatforms = "None listed.";
        }
        platformListingView.setText(allplatforms);
        platformListingView.setTextColor(this.getResources().getColor(R.color.white));
    }

    public void loadImage(ImageHolder holder) {
        final String CACHE_KEY_SUFFIX = "detail";
        String key = holder.getResultKey() + CACHE_KEY_SUFFIX;
        Bitmap bitmap = BitmapCache.getInstance().getBitmapFromMemCache(key);
        if (bitmap == null) {
            Log.d(TAG, "DETAIL BITMAP: DOWNLOADING: " + key);
            ImageDownloadTask task = new ImageDownloadTask(this);
            task.execute(holder);
        } else {
            Log.d(TAG, "DETAIL BITMAP: LOADING FROM CACHE: " + key);
            holder.getImageView().setImageBitmap(bitmap);
        }
    }

    /**
     * ImageDownloadTask Method. Receives the bitmap.
     * @param bitmap
     * @param holder - contains information regarding the result and image view
     */
    @Override public void downloadComplete(Bitmap bitmap, ImageHolder holder) {
        ImageView imageView = holder.getImageView();

        if (bitmap == null) {
            imageView.setImageResource(R.drawable.placeholder);
        } else {
            imageView.setImageBitmap(bitmap);
        }
    }



}
