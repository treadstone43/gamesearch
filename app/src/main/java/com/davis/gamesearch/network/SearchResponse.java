package com.davis.gamesearch.network;

public abstract class SearchResponse {
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
