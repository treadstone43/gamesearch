package com.davis.gamesearch.network;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * This generic result class is here so that no matter where we get the data from, we won't have to
 * change the UI later on. If we were to directly reference the GiantBombResult in the UI classes,
 * it would be too tightly coupled.
 */
public class Result implements Parcelable {

    private String name;
    private String releaseDate;
    private String iconURL;
    private String pictureURL;
    private String id;
    private String description;
    private String siteURL;
    private ArrayList<String> platforms;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getIconURL() {
        return iconURL;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Result() {
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSiteURL() {
        return siteURL;
    }

    public void setSiteURL(String siteURL) {
        this.siteURL = siteURL;
    }


    public ArrayList<String> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(ArrayList<String> platforms) {
        this.platforms = platforms;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.releaseDate);
        dest.writeString(this.iconURL);
        dest.writeString(this.pictureURL);
        dest.writeString(this.id);
        dest.writeString(this.description);
        dest.writeString(this.siteURL);
        dest.writeSerializable(this.platforms);
    }

    private Result(Parcel in) {
        this.name = in.readString();
        this.releaseDate = in.readString();
        this.iconURL = in.readString();
        this.pictureURL = in.readString();
        this.id = in.readString();
        this.description = in.readString();
        this.siteURL = in.readString();
        this.platforms = (ArrayList<String>) in.readSerializable();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
}
