package com.davis.gamesearch.network.imageloader;

import android.widget.ImageView;

import com.davis.gamesearch.network.Result;

/**
 * This class acts as a wrapper to link ImageViews with their respective Result objects
 * without having to carry around all the extra information in the 'Result' object.
 */
public class ImageHolder {
    private ImageView imageView;
    private String imageURL;
    private String resultKey;

    public ImageHolder(Result result, ImageView imageView) {
        this.imageView = imageView;
        this.imageURL = result.getIconURL();
        this.resultKey = result.getId();
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getResultKey() {
        return resultKey;
    }

    public ImageView getImageView() {

        return imageView;
    }
}
