package com.davis.gamesearch.network;

import java.util.ArrayList;

/**
 * Abstracting the SearchEngine class from GiantBombSearchEngine allows us the
 * possibility of later switching to a different search engine with minimal change
 * to the codebase. The Search Activity only cares that it receives 'Result' objects
 * regardless of where they came from.
 */
public abstract class SearchEngine {

    private Listener listener;

    public SearchEngine(Listener listener) {
        this.listener = listener;
    }

    public abstract void fetchResults(String query);

    public interface Listener {
        public void searchComplete(ArrayList<Result> results);
        public void searchError(String error);
    }

}
