package com.davis.gamesearch.network.imageloader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.URL;

/**
 * ImageDownloadTask
 * This task is called whenever a thumbnail image needs to be (re)downloaded.
 */
public class ImageDownloadTask extends AsyncTask<ImageHolder, Void, Bitmap> {

    private ImageDownloadListener listener;
    private ImageHolder holder;

    public interface ImageDownloadListener {
        public void downloadComplete(Bitmap bitmap, ImageHolder holder);
    }

    public ImageDownloadTask(ImageDownloadListener listener) {
        this.listener = listener;
    }

    @Override
    protected Bitmap doInBackground(ImageHolder... holders) {
        holder = holders[0];
        Bitmap bitmap = null;
        Log.v("ImageDownloadTask", "Downloading image from: " + holder.getImageURL());
        try {
            URL imageURL = new URL(holder.getImageURL());
            bitmap = BitmapFactory.decodeStream(imageURL.openStream());
            Log.v("ImageDownloadTask", "Download Successful: bitmap -> " + bitmap.toString());
        } catch (IOException e) {
            Log.e("ImageDownloadTask" + " error", "Downloading Image Failed");
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        Log.v("ImageDownloadTask", "onPostExecute");
        listener.downloadComplete(bitmap, holder);
    }


}
