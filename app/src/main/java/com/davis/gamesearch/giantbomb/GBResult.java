package com.davis.gamesearch.giantbomb;

public class GBResult {

    private int id;
    private BGImage image;
    private String name;
    private int number_of_reviews;
    private String original_release_date;
    private String site_detail_url;
    private String deck;
    private Platform[] platforms;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BGImage getImage() {
        return image;
    }

    public void setImage(BGImage image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber_of_reviews() {
        return number_of_reviews;
    }

    public void setNumber_of_reviews(int number_of_reviews) {
        this.number_of_reviews = number_of_reviews;
    }

    public String getOriginal_release_date() {
        return original_release_date;
    }

    public void setOriginal_release_date(String original_release_date) {
        this.original_release_date = original_release_date;
    }

    public String getSite_detail_url() {
        return site_detail_url;
    }

    public void setSite_detail_url(String site_detail_url) {
        this.site_detail_url = site_detail_url;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public Platform[] getPlatforms() {
        return platforms;
    }

    public void setPlatforms(Platform[] platforms) {
        this.platforms = platforms;
    }
}
