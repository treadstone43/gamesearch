package com.davis.gamesearch.giantbomb;

import android.util.Log;
import android.util.TimingLogger;

import com.davis.gamesearch.network.HttpRequestHandler;
import com.davis.gamesearch.network.Result;
import com.davis.gamesearch.network.SearchEngine;
import com.davis.gamesearch.util.Util;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.UUID;

public class GiantBombSearchEngine extends SearchEngine implements HttpRequestHandler.NetworkListener {

    private static final String TAG = GiantBombSearchEngine.class.getSimpleName();

    private static final String GIANT_BOMB_ENDPOINT = "http://www.giantbomb.com/api";
    private static final String SEARCH_PARAM = "/search";
    private static final String API_KEY_PARAM = "/?api_key=";
    private static final String ACTUAL_API_KEY = "1b31649933de833db3114db11bea71eb8141057d";
    private static final String FORMAT_JSON_PARAM = "&format=json";
    private static final String QUERY_PARAM = "&query=";
    private static final String RESOURCES_PARAM = "&resources=game";
    private static final String RESPONSE_OK = "OK";

    private Listener searchListener;
    private TimingLogger timings;

    public GiantBombSearchEngine(Listener searchListener) {
        super(searchListener);
        this.searchListener = searchListener;
    }

    @Override public void fetchResults(String query) {
        this.timings = new TimingLogger(TAG, "Network Request");
        Log.d(TAG, "Is Loggable? " + Log.isLoggable(TAG, Log.VERBOSE));
        HttpRequestHandler handler = new HttpRequestHandler();
        handler.setListener(this);
        String encodedQuery = null;
        try {
            encodedQuery = URLEncoder.encode("\"" + query + "\"", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringBuilder requestBuilder = new StringBuilder(100);
        requestBuilder.append(GIANT_BOMB_ENDPOINT)
                .append(SEARCH_PARAM)
                .append(API_KEY_PARAM)
                .append(ACTUAL_API_KEY)
                .append(FORMAT_JSON_PARAM)
                .append(QUERY_PARAM)
                .append(encodedQuery)
                .append(RESOURCES_PARAM);

        String request = requestBuilder.toString();
        Log.d(TAG, "Request: " + requestBuilder.toString());

        URI uri = null;
        try {
            uri = new URI(request);
            handler.execute(uri.toASCIIString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert the network json response into POJO
     *
     * @param result
     */
    @Override public void networkRequestCompleted(String result) {
        Log.d(TAG, "Result: " + result);
        timings.addSplit("Response Received");

        if (Util.isNotNullOrEmpty(result)) {
            Gson gson = new Gson();
            GBSearchResponse response = gson.fromJson(result, GBSearchResponse.class);
            timings.addSplit("Json Parsed");

            boolean success = true;
            String errorMessage = null;
            if (!response.getError().equals(RESPONSE_OK)) {
                success = false;
                errorMessage = (Util.isNullOrEmpty(response.getError()) ? "Error in Response" : response.getError());
            }
            if (success && response.getNumber_of_total_results() < 1) {
                success = false;
                errorMessage = "No results found.";
            }

            if (success) {
                ArrayList<Result> results = convertResponseToResults(response);
                searchListener.searchComplete(results);
            } else {
                searchListener.searchError(errorMessage);
            }

            timings.addSplit("Results created");
            timings.dumpToLog();
        }
    }

    /**
     * Developer Note: Should abstract these types of conversion methods to a
     * 'GiantBombPopulator' class. For the sake of time, it is here.
     * @param response
     * @return
     */
    private ArrayList<Result> convertResponseToResults(GBSearchResponse response) {

        ArrayList<Result> results = new ArrayList<Result>();

        for (GBResult gbResult : response.getResults()) {
            Result result = new Result();
            if (Util.isNotNullOrEmpty(gbResult.getName())) {
                result.setName(gbResult.getName());
            } else {
                result.setName("N/A");
            }

            if (Util.isNotNullOrEmpty(gbResult.getOriginal_release_date())) {
                result.setReleaseDate(gbResult.getOriginal_release_date());
            } else {
                result.setReleaseDate("N/A");
            }
            if (gbResult.getImage() != null) {
                result.setIconURL(gbResult.getImage().getIcon_url());
                result.setPictureURL(gbResult.getImage().getSuper_url());
            } else {
                result.setIconURL("N/A");
            }

            result.setDescription(gbResult.getDeck());
            result.setSiteURL(gbResult.getSite_detail_url());
            ArrayList<String> pfNames = new ArrayList<String>();
            if (gbResult.getPlatforms() != null) {
                for (Platform platform : gbResult.getPlatforms()) {
                    pfNames.add(platform.getName());
                }
            }
            result.setPlatforms(pfNames);

            if (gbResult.getId() != 0) {
                result.setId(String.valueOf(gbResult.getId()));
            } else {
                UUID randomId = UUID.randomUUID();
                result.setId(String.valueOf(randomId));
            }
            results.add(result);
        }
        return results;
    }

}
