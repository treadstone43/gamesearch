package com.davis.gamesearch.giantbomb;

import com.davis.gamesearch.network.SearchResponse;

public class GBSearchResponse extends SearchResponse {

    private int limit;
    private int number_of_total_results;
    private GBResult[] results;
    private double version;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getNumber_of_total_results() {
        return number_of_total_results;
    }

    public void setNumber_of_total_results(int number_of_total_results) {
        this.number_of_total_results = number_of_total_results;
    }

    public GBResult[] getResults() {
        return results;
    }

    public void setResults(GBResult[] results) {
        this.results = results;
    }

    public double getVersion() {
        return version;
    }

    public void setVersion(double version) {
        this.version = version;
    }
}
